# videgrenier_install

Contient les scripts d'installations / de mise à jour des machines dockers

## Projet liés

- [Repository du code web](https://gitlab.com/LaMouettas/MonVideGrenier2)


## Instalation

- Cloner ce projet sur la machine linux mint hebergeant le site web
- Ouvrir le fichier .env et ajouter les différents paramètres (Nom de la base de données, mot de passe, ...)
- Ouvrir un terminal et ce placer dans le repertoire du repo
- lancer la commande suivante :
```shell
sudo sh install.sh
```
- Après un certains temps (Mise à jour os, téléchargement des dépendances, lancement du docker), le site internet sera disponible à l'adresse renvoyée par le script.
- En cas de dysfonctionnement pour savoir sur quelle addresse ip le serveur web est hebergé il faudra lancer la commande :
```shell
docker network inspect --format='{{range .Containers}}{{.IPv4Address}}{{end}}' videgrenier_install_netapache | cut -d "/" -f 1
```

## Mise à jour de l'application

- Ouvrir un terminal au niveau du repo cloné (contenant le docker-compose.yml)
- Lancer la commande suivante pour mettre à jour le site web
```shell
sudo sh update.sh
```
- (Facultatif) Lancer la commande suite pour mettre à jour l'ensemble des containers'
```shell
sudo docker-compose up -d --force-recreate
```

## Hard reset docker (!!!)
Pour réinitialiser l'ensemble des containers, network, volumes et images :
- Ouvrir un terminal au niveau du repo cloné (contenant le docker-compose.yml)
- Lancer la commande suivante
```shell
sudo sh reset_docker.sh
```
#!/bin/bash

# Updates
echo "Mise à jour du catalogue de paquets ..."
apt-get update -y > /dev/null
apt-get upgrade -qy > /dev/null

# Intallation of tools to install docker and git
echo "Installation des outils nécessaires à Docker et git ..."
apt-get install -qy \
    ca-certificates \
    curl \
    gnupg \
    lsb-release \
    git > /dev/null

# Docker installation
echo "Installation de Docker ..."
apt-get install -qy docker.io docker-compose > /dev/null

# Lancement du docker-compose
echo "Création du container ..."
docker login registry.gitlab.com
docker-compose up -d --quiet-pull > /dev/null

# Récupération de l'adresse Ip du container web 
ip_container=$(docker network inspect --format='{{range .Containers}}{{.IPv4Address}}{{end}}' videgrenier_install_netapache | cut -d "/" -f 1)
echo "Le site web est disponible localement à cette addresse : $ip_container"

ip_container=$(docker network inspect --format='{{range .Containers}}{{.IPv4Address}}{{end}}' videgrenier_install_netapache | cut -d "/" -f 1)
echo "Le site web est disponible localement à cette addresse : $ip_container"
